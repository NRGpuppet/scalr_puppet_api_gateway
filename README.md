Scalr Puppet API Gateway
========================

Rudimentary python request handler for dispatching Puppet operations via Scalr webhooks, e.g.
updating a puppet node's certificate on a Puppet CA and updating Hiera YAML for a given node.

This app expects the following in the Scalr webhook payload data:

 + SCALR_EVENT_NAME: provided by Scalr
 + SCALR_EVENT_SERVER_HOSTNAME: provided by Scalr
 + SCALR_IS_SUSPEND: provided by Scalr

This script will assume the FQDN of the puppet node (whose cert is being updated) be one of the
following values:

 + SCALR_EVENT_SERVER_HOSTNAME if that values contains at least one period '.'
 + SCALR_EVENT_SERVER_HOSTNAME.ec2.internal otherwise

Note this script assumes any value discovered in SCALR_EVENT_SERVER_HOSTNAME containing at least
one period is a FQDN, but it does not validate that.  A custom value for 
SCALR_EVENT_SERVER_HOSTNAME may be specified in Scalr as Server Hostname Format.

The application integrates the following:

  + Signature validation
  + Request parsing

Copied from https://github.com/scalr-tutorials/webhooks-python-example

Install as Service on CentOS 7
==============================

TODO: Fix gunicorn syslog, not working.

Create dedicated user and copy Python app there:
```
$ sudo useradd -s /usr/sbin/nologin gunicorn
$ sudo git clone https://bitbucket.org/NRGpuppet/scalr_puppet_api_gateway /home/gunicorn/puppet_api_gateway
$ sudo chown -R gunicorn.gunicorn /home/gunicorn/puppet_api_gateway
```

Create startup environment /etc/sysconfig/puppet_api_gateway with the SIGNING_KEY
created for Scalr webhook and the path to a checked out copy of the puppet_control repo:
```
SIGNING_KEY=...
CONTROL_REPO_PATH=/home/gunicorn/puppet_control
```

Create systemd service definition /etc/systemd/system/puppet_api_gateway.service:
```
[Unit]
Description=Gunicorn instance to serve puppet_api_gateway
After=network.target

[Service]
PIDFile=/home/gunicorn/gunicorn.pid
Type=forking
User=gunicorn
Group=gunicorn
WorkingDirectory=/home/gunicorn/puppet_api_gateway
EnvironmentFile=/etc/sysconfig/puppet_api_gateway
ExecStart=/usr/bin/gunicorn -D -p /home/gunicorn/gunicorn.pid --log-file /var/log/gunicorn/puppet_api_gateway.log --capture-output --bind 0.0.0.0:5000 app:app
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s QUIT $MAINPID

[Install]
WantedBy=multi-user.target
```

Open firewall port 5000 and give guincorn user limited sudo access.
```
$ sudo firewall-cmd --permanent --zone=public --add-port=5000/tcp
$ sudo firewall-cmd --reload
$ echo 'gunicorn ALL=(ALL) NOPASSWD: /opt/puppetlabs/bin/puppet' | sudo tee -a /etc/sudoers
$ echo 'gunicorn ALL=(ALL) NOPASSWD: /opt/puppetlabs/bin/hiera' | sudo tee -a /etc/sudoers
$ echo 'gunicorn ALL=(ALL) NOPASSWD: /opt/puppetlabs/puppet/bin/r10k' | sudo tee -a /etc/sudoers
```

Create log directory.
```
$ sudo mkdir /var/log/gunicorn/
$ sudo chown gunicorn.gunicorn /var/log/gunicorn/
```

Finally, enable and start the new puppet_api_gateway service.
```
$ sudo systemctl start puppet_api_gateway
$ sudo systemctl enable puppet_api_gateway
```

Reference:
https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-centos-7

Usage
=====

To deploy the app, you can:

   + Add it to an instance you own, install its dependencies
   + Use a PaaS, either deployed on your own infrastructure (CloudFoundry), or on public infrastructure (e.g. Heroku)


Installing Dependencies
-----------------------

To install the app's dependencies, do the following:

  + Ensure that you have the [Python Pip installer][0] available on your system
  + Run `pip install -r requirements.txt`, from the root of the project


Configuring the app
-------------------

The application is configured by passing its configuration through environment variables (i.e. it's a [12-factor app][10]).

If you are using Honcho or Foreman (as suggested below), you can simply input those environment variables in the `.env`
file.

The following environment variables are used:

  + `SIGNING_KEY`: This should be the signing key provided by Scalr, used to authenticate requests.
  + `CONTROL_REPO_PATH`: The path to a checked out copy of the Puppet control repo, suitable for
      gunicorn to push commits from, e.g. /home/gunicorn/puppet_control .

Running the app
---------------

Once you have installed and configured the app, you can launch it by running `honcho start web`. The app will listen on
port `5000` by default.

If you're launching the app on an instance, you'll probably want to daemonize it. To do so, navigate to the app
directory, and then run:

    gunicorn --daemon --bind 0.0.0.0:5000 app:app

Bear in mind that `gunicorn` will not read your `.env` file, so you have to pass the `SIGNING_KEY` environment variable
through other means.

One option is:

    SIGNING_KEY=yyyy CONTROL_REPO_PATH=xxxx gunicorn --daemon --bind 0.0.0.0:5000 app:app

To run gunicorn on the console (i.e. for test/debugging), stop CentOS 7 service created above, cd to a cloned repo of this repo (e.g. /home/gunicorn/puppet_api_gateway) and launch gunicorn manually:

    SIGNING_KEY=yyyy CONTROL_REPO_PATH=xxxx /usr/bin/gunicorn --log-level debug --bind 0.0.0.0:5000 app:app

For further information, you should check the [Gunicorn documentation][3].


Further reading
===============

For more information, you should refer to our [Webhooks Documentation][1].


Issues
======

Please report issues and ask questions on the project's Github repository, in the [issues section][2].


License
=======

View `LICENSE`.


  [0]: http://www.pip-installer.org/
  [10]: http://12factor.net/
  [1]: https://scalr-wiki.atlassian.net/wiki/x/FYBe
  [2]: https://github.com/scalr-tutorials/webhooks-python-example/issues
  [3]: http://gunicorn-docs.readthedocs.org/en/latest/configure.html
