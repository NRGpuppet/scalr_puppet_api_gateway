#coding:utf-8

# Rudimentary python request handler for dispatching Puppet operations via Scalr webhooks
#
# This script will assume the FQDN of the puppet node (whose cert is being updated) be one of the
# following values:
#
# - SCALR_EVENT_SERVER_HOSTNAME if that values contains at least one period '.'
# - SCALR_EVENT_SERVER_HOSTNAME.ec2.internal otherwise
#
# Note this script assumes any value discovered in SCALR_EVENT_SERVER_HOSTNAME containing at least
# one period is a FQDN, but it does not validate that.  A custom value for 
# SCALR_EVENT_SERVER_HOSTNAME may be specified in Scalr as Server Hostname Format.

import os
import sys
import subprocess
import hashlib
import hmac
import logging
import time

import flask
import util

SIGNING_KEY_ENV_VAR = "SIGNING_KEY"

app = flask.Flask(__name__)
app.config.update(
    signing_key=os.environ.get(SIGNING_KEY_ENV_VAR, ""),
)

# Logging configuration
stderr_log_handler = logging.StreamHandler()
app.logger.addHandler(stderr_log_handler)
app.logger.setLevel(logging.INFO)

def get_logger_timestamp():
    return "[" + time.strftime("%Y-%m-%d %H:%M:%S +0000", time.gmtime()) + "]"


@app.route("/", methods=("GET",))
def webhook_get_handler():
    return flask.Response(status=200)


@app.route("/", methods=("POST",))
def webhook_post_handler():
    payload = flask.request.json
    app.logger.info("%s Received Notification '%s' for: '%s' on '%s'", get_logger_timestamp(),
                    payload["eventId"], payload["eventName"], payload["data"]["SCALR_SERVER_ID"])

    # Verify payload data exist and are not blank
    if ("SCALR_EVENT_SERVER_HOSTNAME" in payload["data"].keys() and
        "SCALR_EVENT_NAME" in payload["data"].keys() and
        payload["data"]["SCALR_EVENT_SERVER_HOSTNAME"] and
        payload["data"]["SCALR_EVENT_NAME"] ):

        # Determine node's FQDN
        if ('.' in payload["data"]["SCALR_EVENT_SERVER_HOSTNAME"]):
            node = payload["data"]["SCALR_EVENT_SERVER_HOSTNAME"]
        else:
            node = payload["data"]["SCALR_EVENT_SERVER_HOSTNAME"] + '.ec2.internal' 

        # For BeforeHostTerminate event where SCALR_IS_SUSPEND = 0, clean cert
        if (payload["data"]["SCALR_EVENT_NAME"] == "BeforeHostTerminate" and
            "SCALR_IS_SUSPEND" in payload["data"].keys() and
            (payload["data"]["SCALR_IS_SUSPEND"] == "0" or
            payload["data"]["SCALR_IS_SUSPEND"] == "" )):

            # Tell Puppet CA to clean node's cert
            try:
                cmd = "sudo /opt/puppetlabs/bin/puppet cert clean " + node
                subprocess.check_call(cmd, shell=True)
                app.logger.info("%s Successfully cleaned puppert cert for node %s",
                                get_logger_timestamp(), node)
            except subprocess.CalledProcessError:
                app.logger.exception("%s Error performing puppet cert clean for node %s.",
                                     get_logger_timestamp(), node)

        # For HostInit event, update Hiera YAML for this server
        elif (payload["data"]["SCALR_EVENT_NAME"] == "HostInit" and
            payload["data"]["PUPPET_HIERA"]):

            # 1st refresh control repo
            try:
                cmd = "/bin/git --git-dir=" + os.environ.get('CONTROL_REPO_PATH') + "/.git  --work-tree=" + os.environ.get('CONTROL_REPO_PATH') + " pull"
                subprocess.check_call(cmd, shell=True)
                app.logger.info("%s Successfully refreshed puppet_control working copy",
                                get_logger_timestamp())
            except subprocess.CalledProcessError:
                app.logger.exception("%s Error refreshing puppet_control working copy.",
                                     get_logger_timestamp())

            # Next output hiera to file in control repo
            hiera_file = os.environ.get('CONTROL_REPO_PATH') + "/hiera/fqdn/" + node + ".yaml"
            with open(hiera_file, 'w') as f:
                try:
                    f.write(payload["data"]["PUPPET_HIERA"])
                    f.close()
                except:
                    app.logger.exception("%s Error writing %s.",
                                         get_logger_timestamp(), hiera_file)

            # Next add hiera file to control_repo working copy
            try:
                cmd = "/bin/git --git-dir=" + os.environ.get('CONTROL_REPO_PATH') + "/.git  --work-tree=" + os.environ.get('CONTROL_REPO_PATH') + " add hiera/fqdn/" + node + ".yaml"
                subprocess.check_call(cmd, shell=True)
                app.logger.info("%s Successfully added hiera YAML for %s.",
                                get_logger_timestamp(), node)
            except subprocess.CalledProcessError:
                app.logger.exception("%s Error adding hiera YAML for %s.",
                                     get_logger_timestamp(), node)

            # Next commit hiera file
            try:
                cmd = "/bin/git --git-dir=" + os.environ.get('CONTROL_REPO_PATH') + "/.git  --work-tree=" + os.environ.get('CONTROL_REPO_PATH') + " commit -m \"Scalr adding hiera for " + node + "\" hiera/fqdn/" + node + ".yaml"
                subprocess.check_call(cmd, shell=True)
                app.logger.info("%s Successfully committed hiera YAML for %s.",
                                get_logger_timestamp(), node)
            except subprocess.CalledProcessError:
                app.logger.exception("%s Error committing hiera YAML for %s.",
                                     get_logger_timestamp(), node)

            # Next push to control_repo
            try:
                cmd = "/bin/git --git-dir=" + os.environ.get('CONTROL_REPO_PATH') + "/.git  --work-tree=" + os.environ.get('CONTROL_REPO_PATH') + " push origin production"
                subprocess.check_call(cmd, shell=True)
                app.logger.info("%s Successfully pushed hiera YAML for %s.",
                                get_logger_timestamp(), node)
            except subprocess.CalledProcessError:
                app.logger.exception("%s Error pushed hiera YAML for %s.",
                                     get_logger_timestamp(), node)

            # Finally update puppetserver's working copy
            try:
                cmd = "sudo /opt/puppetlabs/puppet/bin/r10k deploy environment production"
                subprocess.check_call(cmd, shell=True)
                app.logger.info("%s Successfully updated puppetserver environment.",
                                get_logger_timestamp())
            except subprocess.CalledProcessError:
                app.logger.exception("%s Error updating puppetserver environment.",
                                     get_logger_timestamp())

        else:
            app.logger.info("%s Nothing to do for event '%s'", get_logger_timestamp(),
                            payload["data"]["SCALR_EVENT_NAME"])

    return flask.Response(status=202)


@app.before_request
def log_request():
    app.logger.debug("%s Received request: %s %s", get_logger_timestamp(), flask.request.method,
                     flask.request.url)


@app.before_request
def validate_request_signature():
    if flask.request.method == "GET":
        return

    signing_key = app.config["signing_key"]
    if signing_key is None:
        app.logger.warning("%s No signing key found. Request will not be checked for authenticity.",
                           get_logger_timestamp())
        return

    payload = flask.request.get_data()
    date = flask.request.headers.get("Date", "")
    message_hmac = hmac.HMAC(signing_key, payload + date, hashlib.sha1)

    local_signature = message_hmac.hexdigest()
    remote_signature = flask.request.headers.get("X-Signature", "")

    if not util.constant_time_compare(local_signature, remote_signature):
        app.logger.warning("%s Detected invalid signature, aborting.", get_logger_timestamp())
        return flask.Response(status=403)


@app.before_request
def validate_json_payload():
    if flask.request.method == "GET":
        return
    if flask.request.json is None:
        return flask.Response(status=400)


if __name__ == '__main__':
    app.run(debug=True)
